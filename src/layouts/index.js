import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import Menu from '../components/menu'
import Header from '../components/header'
import './index.scss'
import 'bootstrap/dist/css/bootstrap.min.css'
import Footer from '../components/footer'
import './style.css'
import icon32 from '../../assets/Kolega_Coworking_Space.png'

const Layout = ({ children, data }) => (
  <div>
    <Helmet
      title={data.site.siteMetadata.title}
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
      link={[
        {rel: 'shortcut icon', type: 'image/png', href: `${icon32}`}
      ]}
    />
    {/*<Header siteTitle={data.site.siteMetadata.title} /> */}
    <Menu />
    <div>
      {children()}
    </div>
    <Footer />
  </div>
)

Layout.propTypes = {
  children: PropTypes.func,
}

export default Layout

export const query = graphql`
  query SiteTitleQuery {
    site {
      siteMetadata {
        title
      }
    }
  }
`
