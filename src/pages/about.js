import React from 'react'
import HeroSingle from '../components/heroSingle';
import AboutStory from '../components/aboutStory';
import HeroLink from '../components/heroLink';
import BigImage from '../components/bigImage';
import AboutTeam from '../components/aboutTeam';
import aboutHeaderImg from '../../assets/about-header.jpg'
import stockTimeImg from '../../assets/stock-and-time.jpg'
import Sticky from 'react-stickynode'

//const ourTeamText = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";
//const ourStoryText = "Starting in 2015, we see that the community;s need a place of work with a different concept than the conventional workplace We believe that a flexible office environment can increase productivity. Our purpose is to gather and connect people that have similar interests and create a network of collaboration. Let's collaborate!";

const stickyBtnStyle = {
  backgroundColor: '#fad02c',
  width: '120px',
  padding: '10px',
  position: 'absolute',
  right: '0px',
  zIndex: '999',
}

const aboutPage = ({data}) => {
  return (
    <div>
      <Sticky enabled={true} top={50} bottomBoundary='#highlightGrid' innerZ={500}>
        <div style={stickyBtnStyle}>
          <a href="/contact">CONTACT US</a>
        </div>
      </Sticky>
      <HeroSingle backgroundImage={aboutHeaderImg} />
      <AboutStory ourStoryText={data.allPrismicAboutPage.edges[0].node.data.story.html} />
      <HeroLink />
      <BigImage bigImageSrc={stockTimeImg} />
      <AboutTeam ourTeamText={[data.allPrismicAboutPage.edges[0].node.data.team.html]} />
      {/*<AboutTeam ourTeamText={[<p>halo</p>]} />*/}
    </div>
  )
}

export default aboutPage;

export const query = graphql`
query AboutQuery {
  allPrismicAboutPage {
    edges {
      node {
        id 
        data {
          story {
            html
          }
          team {
            html
          }
        }
      }
    }
  }
}
`