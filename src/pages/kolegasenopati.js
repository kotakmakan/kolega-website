import React from 'react'
import Link from 'gatsby-link'
import Features from '../components/features'
import LocationHeadline from '../components/locationHeadline'
import LocationOffer from '../components/locationOffer'
import LocationPrice from '../components/locationPrice'
import Hero from '../components/hero';
import LocationHero1 from '../../assets/foto-per-lokasi/Senopati/foto-senopati-1.jpg'
import LocationHero2 from '../../assets/foto-per-lokasi/Senopati/foto-senopati-2.jpg'
import LocationHero3 from '../../assets/foto-per-lokasi/Senopati/foto-senopati-3.jpg'
import LocationHero4 from '../../assets/foto-per-lokasi/Senopati/foto-senopati-4.jpg'
import ResponsiveEmbed from 'react-responsive-embed'
import LocationList from '../components/locationList';
import Sticky from 'react-stickynode'

let description = "Kolega x Markplus is the start of a fundamental collaboration between Kolega Coworking Space and Markplus. Here, we are giving you the experience of working with a really great environment. Book your office here for an exciting journey!"
const plansList = ['Daily Access', 'Coworking Area', 'Dedicated Desk', 'Office Suites'];
const plansPriceList = ['Rp 110.000,- / day', 'Rp 1.100.000,- / month', 'Rp 2.500.000,- / month', 'Starts from Rp 15.000.000,- / month']
const roomsList = ['Small (6 persons)', 'Medium (8 persons)', 'Large(16 persons)'];
const roomsPriceList = ['not available', 'Rp 330.000,- / 2 hours', 'not available'];
const spacesList = ['Closed for Public Events', 'Open for Public Events'];
const spacesPriceList = ['Rp 1.650.000,- / 2 hours', 'Rp 1.100.000,- / 2 hours'];
const sliderItems = [
  {
    src: LocationHero1,
    caption: 'Jl. Suryo no. 50, Senopati, Kebayoran Baru, Jakarta 12180',
    header: 'Kolega Senopati'
  },
  {
    src: LocationHero2,
    caption: 'Jl. Suryo no. 50, Senopati, Kebayoran Baru, Jakarta 12180',
    header: 'Kolega Senopati'
  },
  {
    src: LocationHero3,
    caption: 'Jl. Suryo no. 50, Senopati, Kebayoran Baru, Jakarta 12180',
    header: 'Kolega Senopati'
  },
  {
    src: LocationHero4,
    caption: 'Jl. Suryo no. 50, Senopati, Kebayoran Baru, Jakarta 12180',
    header: 'Kolega Senopati'
  }
];

const stickyBtnStyle = {
  backgroundColor: '#fad02c',
  width: '120px',
  padding: '10px',
  position: 'absolute',
  right: '0px',
  zIndex: '999',
}

const KolegaSenopatiPage = () => (
  <div>
    <Sticky enabled={true} top={50} bottomBoundary='#highlightGrid' innerZ={500}>
      <div style={stickyBtnStyle}>
        <a href="/contact">CONTACT US</a>
      </div>
    </Sticky>
    <LocationHeadline />
    <Hero items={sliderItems}/>
    <LocationOffer description={description} />
    <div className="columns" style={{marginBottom: "50px"}}>
      <div className="column is-6 is-offset-3">
        <ResponsiveEmbed src="https://www.youtube.com/embed/FiaCbn6a5GY" allowFullScreen />
      </div>
    </div>
    <div className="columns">
      <div className="column is-half">
        <LocationPrice 
          plans={plansList} 
          plansPrice={plansPriceList}
          rooms={roomsList} 
          roomsPrice={roomsPriceList} 
          spaces={spacesList} 
          spacesPrice={spacesPriceList}
        />
      </div>
      <div className="column is-half" id="map-container">
        <div className="mapouter">
          <div className="gmap_canvas">
            <iframe width="960" height="700" id="gmap_canvas" src="https://maps.google.com/maps?q=kolega%20senopati&t=&z=13&ie=UTF8&iwloc=&output=embed" frameBorder="0" scrolling="no" marginHeight="0" marginWidth="0"></iframe>
          </div>
        </div>
      </div>  
    </div>
    <Features />
    <LocationList />
  </div>
)

export default KolegaSenopatiPage
