import React from 'react'
import Hero from '../components/hero';
import PlansCard from '../components/plansCard';
import LocationHero1 from '../../assets/plans/foto-plan-1.jpg'
import LocationHero2 from '../../assets/plans/foto-plan-2.jpg'
import LocationHero3 from '../../assets/plans/foto-plan-3.jpg'
import allAccessCardPlanImg from '../../assets/plans/plan-1.png'
import coworkingAreaPlanImg from '../../assets/plans/plan-2.png'
import dedicatedDeskPlanImg from '../../assets/plans/plan-3.png'
import officeSuitesPlanImg from '../../assets/plans/plan-4.png'
import meetingRoomPlanImg from '../../assets/plans/plan-5.png'
import eventSpacesPlanImg from '../../assets/plans/plan-6.png'
import Sticky from 'react-stickynode'

const sliderItems = [
  {
    src: LocationHero1,
    caption: 'Jl. Pangeran Antasari no.36, Puri Sakti Buntu I, Jakarta Selatan',
    header: 'Kolega Antasari'
  },
  {
    src: LocationHero2,
    caption: 'Jl. Pangeran Antasari no.36, Puri Sakti Buntu I, Jakarta Selatan',
    header: 'Kolega Antasari'
  },
  {
    src: LocationHero3,
    caption: 'Jl. Pangeran Antasari no.36, Puri Sakti Buntu I, Jakarta Selatan',
    header: 'Kolega Antasari'
  }
];

const allAccessCardContent = {
  headlineImage: allAccessCardPlanImg,
  headlineTitle: 'Kolega All Access Card',
  subtitle: '',
  linkText: 'Register Now',
  linkDest: '/contact',
  intro: "Working in the city means that you need a lot of options for places to work. With this plan, you could access Kolega locations anywhere, anytime! Make your day more efficient by working in the nearest Kolega from your place, or meet with clients.",
  header: "PRIVILEGE",
  texts: [
    "Become an Exclusive Member of Kolega's Community",
    "Get A Cool Membership Card*",
    "Have access to partner benefits*",
    "Work Anytime, Anywhere, Your Call",
    "Access to all events held by Kolega",
    "Get specials prices forever"
  ],
  note: "*Terms & Conditions Applied"
}

const coworkingAreaContent = {
  headlineImage: coworkingAreaPlanImg,
  headlineTitle: 'Coworking Area',
  subtitle: 'IDR 1.000.000 / month',
  linkText: 'Browse Location',
  linkDest: '/locations',
  intro: "Join Kolega Coworking Space as a member of the growing community. Access to the coworking area in your preferred location with no assigned desk. Get a fresh new desk every day",
  header: "BEST FOR",
  texts: [
    "Remote and part time workers",
    "Client meetings",
    "More than a week per month"
  ]
}

const dedicatedDeskContent = {
  headlineImage: dedicatedDeskPlanImg,
  headlineTitle: 'Kolega All Access Card',
  subtitle: 'IDR 2.000.000 / month',
  linkText: 'Browse Location',
  linkDest: '/locations',
  intro: "Access to the coworking area and dedicated area with one assigned desk for the period of this membership. Get together with your colleagues and fellow coworkers, while having a personalized desk to organize your business.",
  header: "BEST FOR",
  texts: [
    "Startups and small agencies",
    "Collaboration and growth",
    "Everyday Use"
  ]
}

const officeSuitesContent = {
  headlineImage: officeSuitesPlanImg,
  headlineTitle: 'Kolega All Access Card',
  subtitle: 'Starts from IDR 5.000.000 / unit',
  linkText: 'Browse Location',
  linkDest: '/locations',
  intro: "Access to the selected, tailor made, private office suite(s) for you and your team,  while being able to access the coworking area to huddle up with the community and colleagues.",
  header: "BEST FOR",
  texts: [
    "Startups and small agencies",
    "Collaboration and growth",
    "Everyday Use"
  ]
}

const meetingRoomContent = {
  headlineImage: meetingRoomPlanImg,
  headlineTitle: 'MEETING ROOM',
  subtitle: 'Starts from IDR 165.000 / 2 hours',
  linkText: 'Browse Location',
  linkDest: '/locations',
  intro: "Need a comfy place to do your meetings? We have meeting rooms that could fit 4 to 30 persons each room.",
  header: "INCLUDING",
  texts: [
    "Internet Connection",
    "Self service mineral water/coffee/tea",
    "White board"
  ]
}

const eventSpacesContent = {
  headlineImage: eventSpacesPlanImg,
  headlineTitle: 'EVENT SPACES',
  subtitle: 'Starts from IDR 1.100.000 / 2 hours',
  linkText: 'Browse Location',
  linkDest: '/locations',
  intro: "For your event needs, we have modular spaces that could accommodate up to 70 persons capacity events. In the spaces, you will find inspiration and experience",
  header: "INCLUDING",
  texts: [
    "Startups and small agencies",
    "Collaboration and growth",
    "Everyday Use"
  ]
}

const stickyBtnStyle = {
  backgroundColor: '#fad02c',
  width: '120px',
  padding: '10px',
  position: 'absolute',
  right: '0px',
  zIndex: '999',
}

const plansPage = () => {
  return (
    <div>
      <Sticky enabled={true} top={50} bottomBoundary='#highlightGrid' innerZ={500}>
        <div style={stickyBtnStyle}>
          <a href="/contact">CONTACT US</a>
        </div>
      </Sticky>
      <Hero items={sliderItems} />
      <div className="columns" style={{marginTop: '30px'}}>
        <div className="column is-10 is-offset-1">
          <div className="columns">
            <div className="column is-half">
              <PlansCard content={allAccessCardContent}/>
            </div>
            <div className="column is-half">
              <PlansCard content={coworkingAreaContent} />
            </div>
          </div>

          <div className="columns">
            <div className="column is-half">
              <PlansCard content={dedicatedDeskContent} />
            </div>
            <div className="column is-half">
              <PlansCard content={officeSuitesContent} />
            </div>
          </div>

          <div className="columns">
            <div className="column is-half">
              <PlansCard content={meetingRoomContent} />
            </div>
            <div className="column is-half">
              <PlansCard content={eventSpacesContent} />
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default plansPage