import React from 'react'
import Link from 'gatsby-link'
import Features from '../components/features';
import HeroSliderLocation from '../components/heroSliderLocation';
import LocationList from '../components/locationList';
import LocationImages from '../components/locationImages.js'
import Sticky from 'react-stickynode'

const stickyBtnStyle = {
  backgroundColor: '#fad02c',
  width: '120px',
  padding: '10px',
  position: 'absolute',
  right: '0px',
  zIndex: '999',
}

const LocationPage = () => (
  <div>
    <Sticky enabled={true} top={50} bottomBoundary='#highlightGrid' innerZ={500}>
      <div style={stickyBtnStyle}>
        <a href="/contact">CONTACT US</a>
      </div>
    </Sticky>
    <HeroSliderLocation />
    <LocationImages />
    <LocationList />
    <div style={{padding: "50px 0"}}>
      <Features />
    </div>
  </div>
)

export default LocationPage
