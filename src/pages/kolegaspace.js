import React from 'react'
import Link from 'gatsby-link'
import Features from '../components/features'
import LocationHeadline from '../components/locationHeadline'
import LocationOffer from '../components/locationOffer'
import LocationPrice from '../components/locationPrice'
import Hero from '../components/hero';
import LocationHero1 from '../../assets/foto-per-lokasi/Tebet/foto-tebet-1.jpg'
import LocationHero2 from '../../assets/foto-per-lokasi/Tebet/foto-tebet-2.jpg'
import LocationHero3 from '../../assets/foto-per-lokasi/Tebet/foto-tebet-3.jpg'
import PlaceholderMap from '../../assets/Placeholder peta.jpg'
import ResponsiveEmbed from 'react-responsive-embed'
import LocationList from '../components/locationList';
import Sticky from 'react-stickynode'

let description = "Kolega x Markplus is the start of a fundamental collaboration between Kolega Coworking Space and Markplus. Here, we are giving you the experience of working with a really great environment. Book your office here for an exciting journey!"
const plansList = ['Daily Access', 'Coworking Area', 'Dedicated Desk', 'Office Suites'];
const plansPriceList = ['Rp -', 'Rp -', 'Rp -', 'Rp -']
const roomsList = ['Small (6 persons)', 'Medium (8 persons)', 'Large(16 persons)'];
const roomsPriceList = ['Rp -', 'Rp -', 'Rp -'];
const spacesList = ['Closed for Public Events', 'Open for Public Events'];
const spacesPriceList = ['Rp -', 'Rp -'];
const sliderItems = [
  {
    src: LocationHero1,
    caption: '',
    header: ''
  },
  {
    src: LocationHero2,
    caption: '',
    header: ''
  },
  {
    src: LocationHero3,
    caption: '',
    header: ''
  },
];

const stickyBtnStyle = {
  backgroundColor: '#fad02c',
  width: '120px',
  padding: '10px',
  position: 'absolute',
  right: '0px',
  zIndex: '999',
}

const KolegaSpacePage = () => (
  <div>
    <Sticky enabled={true} top={50} bottomBoundary='#highlightGrid' innerZ={500}>
      <div style={stickyBtnStyle}>
        <a href="/contact">CONTACT US</a>
      </div>
    </Sticky>
    <LocationHeadline />
    <Hero items={sliderItems}/>
    <LocationOffer description={description} />
    <div className="columns" style={{marginBottom: "50px"}}>
      <div className="column is-6 is-offset-3">
        <ResponsiveEmbed src="https://www.youtube.com/embed/FiaCbn6a5GY" allowFullScreen />
      </div>
    </div>
    <div className="columns">
      <div className="column is-half">
        <LocationPrice 
          plans={plansList} 
          plansPrice={plansPriceList}
          rooms={roomsList} 
          roomsPrice={roomsPriceList} 
          spaces={spacesList} 
          spacesPrice={spacesPriceList}
        />
      </div>
      <div className="column is-half" id="map-container">
        <div className="mapouter">
          <img src={PlaceholderMap} />
        </div>
      </div>  
    </div>
    <Features />
    <LocationList />
  </div>
)

export default KolegaSpacePage
