import React from 'react';
import ContactFormHeader from '../../assets/Contact us.png'
import MemberIcon from '../../assets/member-01.png'
import TenantIcon from '../../assets/tenant-01.png'
import CommunityIcon from '../../assets/community-01.png'
import ContactFormMember from '../components/contactFormMember'
import ContactFormLandlord from '../components/contactFormLandlord'
import ContactformCommunity from '../components/contactFormCommunity'

const imageStyle = {
  width: '50%',
  margin: '0 auto 10px auto',
  display: 'block'
}

export default class ContactPage extends React.Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: 1
    };
  }

  toggle(tab) {
    //if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    //}
  }

  render() {
    console.log(this.state.activeTab);
    let myForm;
    if (this.state.activeTab == 1) {
      myForm = <ContactFormMember />
    } else if (this.state.activeTab == 2) {
      myForm = <ContactFormLandlord />
    } else if (this.state.activeTab == 3) {
      myForm = <ContactformCommunity />
    }
    
    return (
      <div style={{padding: "50px"}}>
        <div className="columns">
        <div className="column is-8 is-offset-2">
        <img src={ContactFormHeader} style={{margin: '0 auto 10px auto', display:'block'}}/>
        <h4 style={{fontFamily: 'RobotoSlab', fontWeight:'bold', textAlign: 'center'}}>WHO ARE YOU?</h4>
        <div id="contactForm" style={{marginTop: '40px'}}>
          <div className="columns">
            <div className="column is-third" style={{textAlign: 'center'}} onClick={() => this.toggle(1)}>
              <a><img src={MemberIcon} style={imageStyle}/><p style={{textTransform:'uppercase'}}>Member</p></a>
            </div>
            <div className="column is-third" style={{textAlign: 'center'}} onClick={() => this.toggle(2)}>
              <a><img src={TenantIcon} style={imageStyle}/><p style={{textTransform:'uppercase'}}>Building Owner</p></a>
            </div>
            <div className="column is-third" style={{textAlign: 'center'}} onClick={() => this.toggle(3)}>
              <a><img src={CommunityIcon} style={imageStyle}/><p style={{textTransform:'uppercase'}}>Community & Partner</p></a>
            </div>
          </div>
          <p>One of the advantages of coworking is networking, getting out of the house gives you the possibility to land new clients and grow your business. And some additional reasons you should consider to join Kolega is having other people around you trying to succeed will boost your motivation and you’ll feel energized to maintain your workflow. Join us and let’s collaborate!
</p>  
          <div id="contactFormMember">
            {myForm}
          </div>
        </div>  
        </div>
        </div>
      </div>
    );
  }
}