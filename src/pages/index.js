import React from 'react'
import Link from 'gatsby-link'
import Hero from '../components/hero'
import Highlight from '../components/highlights'
import GridGallery from '../components/gridGallery';
import SpaceBookingForm from '../components/spaceBookingForm';
import HomepageHero from '../../assets/header-homepage/Header homepage 1.jpg';
import HomepageHero2 from '../../assets/header-homepage/Header homepage 2.jpg';
import HomepageHero3 from '../../assets/header-homepage/Header homepage 3.jpg';
import KolegaLogoBig from '../../assets/kolega-logo-big.png'
import LinkButton from '../components/linkButton';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Sticky from 'react-stickynode';

const sliderItems = [
  {
    src: HomepageHero,
    altText: 'Slide 1',
    /*caption: "Beyond creating a better place to work, we design an ecosystem that embodies friendship as the fuel of productivity and collaboration. Choose your work style, whether it's office, dedicated desk, flexible desk or even a customized one.",
    header: '#GOTONGROYONG'*/
    caption: "",
    header: ""
  },
  {
    src: HomepageHero2,
    altText: 'Slide 2',
    /*caption: "Beyond creating a better place to work, we design an ecosystem that embodies friendship as the fuel of productivity and collaboration. Choose your work style, whether it's office, dedicated desk, flexible desk or even a customized one.",
    header: '#GOTONGROYONG'*/
    caption: "",
    header: ""
  },
  {
    src: HomepageHero3,
    altText: 'Slide 3',
    /*caption: "Beyond creating a better place to work, we design an ecosystem that embodies friendship as the fuel of productivity and collaboration. Choose your work style, whether it's office, dedicated desk, flexible desk or even a customized one.",
    header: '#GOTONGROYONG'*/
    caption: "",
    header: ""
  }
]

const stickyBtnStyle = {
  backgroundColor: '#fad02c',
  width: '120px',
  padding: '10px',
  position: 'absolute',
  right: '0px',
  zIndex: '999',
}

class IndexPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: true
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  componentDidMount() {
    let visited = localStorage["alreadyVisited"];
    if(visited) {
      this.setState({
        modal: false
      }) 
    } 
    else {
      localStorage["alreadyVisited"] = true;
      this.setState({
        modal: true
      })
    }
  }

  render() {
    return (
      <div>
        <Sticky enabled={true} top={50} bottomBoundary='#highlightGrid' innerZ={500}>
          <div style={stickyBtnStyle}>
            <a href="/contact">CONTACT US</a>
          </div>
        </Sticky>
        <div className={"modal " + (this.state.modal ? 'is-active' : '')}>
          <div className="modal-background"></div>
          <div className="modal-content has-text-centered" style={{width:'75%', borderRadius: '0', background: '#ededed'}}>
            <div style={{padding:'5rem 5rem'}}>
              <img src={KolegaLogoBig} style={{maxWidth: '220px', marginBottom: '20px'}}/>
              <h1 style={{color:'#314e70', fontWeight: 'bold'}}>#WELCOMETOKOLEGA</h1>
              <p>A place where you can meet talented people with different backgrounds and you will get tons of opportunity to create an impact.</p>
              <div style={{border: 'solid 1px black', padding:'0.6rem', width: '200px', margin: '20px auto'}}><LinkButton text="CONTACT US" dest="/"/></div>
              <p>Join our Awesome Newsletter. Subscribe to our newsletter to get easy access to our interesting events, workshops, promotions and networking to widen up your professional network.</p>             
              <Form inline className="justify-content-center">
                <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
                  <Label for="newsletterEmail" className="mr-sm-2">Email</Label>
                  <Input type="email" name="newsletterEmail" id="newsletterEmail" placeholder="email@domain.com" />
                </FormGroup>
                <Button>Submit</Button>
              </Form>       
            </div>
          </div>
          <button className="modal-close is-large" onClick={this.toggle}></button>
        </div>

        <div className="centerCaption">
          <Hero items={sliderItems} />
        </div>  
        <div className="columns" style={{padding: "0 0 0 0"}}>
          <div className="column is-6 is-offset-3" style={{padding: "50px"}}>
            <SpaceBookingForm />
          </div>
        </div>
        <Highlight />
        <GridGallery/>
      </div>
    );
  }
}

//export default lifecycle(methods)(IndexPage);
export default IndexPage;