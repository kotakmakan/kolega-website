import React from 'react'

export default () => {
  return (
    <div>
      <div className="columns" id="locationHeadlineWrapper">
        <div className="column is-half">
          <h1>Let's Work Together</h1>
          <p className="is-size-6">Work Amongst the community to stimulate growth and collaboration, while having the choice to have your own private space to do your works.</p>
        </div>
      </div>  
    </div>
  )
}
