import React from 'react';
import { Col, Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import BookingTitleImage from '../../assets/book-a-tour.png'

export default class SpaceBookingForm extends React.Component {
  render() {
    return (
      <div>
        <div className="has-text-centered">
          <img src={BookingTitleImage}  style={{maxWidth: '90%', margin: '0 0 50px 0'}}/>
        </div>
        <Form action="https://formspree.io/info@kolega.co" method="POST" name="_subject" value="New booking request!">
          <FormGroup row>
            <Label for="name" sm={3}>Name</Label>
            <Col sm={9}>
              <Input type="text" name="name" id="formName" placeholder="Name" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="email" sm={3}>Email</Label>
            <Col sm={9}>
              <Input type="email" name="email" id="formEmail" placeholder="Email" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="phone" sm={3}>Phone</Label>
            <Col sm={9}>
              <Input type="phone" name="phone" id="formPhone" placeholder="Phone" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="organization" sm={3}>Organization</Label>
            <Col sm={9}>
              <Input type="text" name="organization" id="formOrganization" placeholder="Organization" />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="needs" sm={3}>Needs</Label>
            <Col sm={9}>
              <Input type="select" name="needs" id="formNeeds"> 
                <option>1-5 person</option>
                <option>6-10 person</option>
                <option>11+ person</option>
              </Input>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label for="date" sm={3}>Date</Label>
            <Col sm={9}>
              <Input type="date" name="date" id="form-date" placeholder="Date" />
            </Col>
          </FormGroup>
          <FormGroup>
            <div style={{textAlign:'center'}}>
              <Button type="submit">Submit</Button>
            </div>
          </FormGroup>
        </Form>
      </div>  
    );
  }
}