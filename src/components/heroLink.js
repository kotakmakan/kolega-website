import React from 'react'
import LinkButton from './linkButton';
import heroLinkImg from '../../assets/about-hub.png'

const HeroLink = () => {
  return (
    <section className="hero is-medium is-bold">
      <div className="hero-body" style={{backgroundImage: `url(${heroLinkImg})`}}>
        <div className="container has-text-centered">
          <h1 className="title">
            See how collaboration started in one of our spaces
          </h1>
          <LinkButton text="Check our collaboration hub" dest="/#"/>
        </div>
      </div>
    </section>
  )
}

export default HeroLink;
