import React, { Component } from 'react'
import './features.css'
import LinkButton from './linkButton';

class AboutFeatures extends Component {
  constructor(props) {
    super()
    this.state = {
      addHourClass: false
    }
  }

  toggleHidden () {
    this.setState({
      addHourClass: !this.state.addClass
    })
  }

  render() {
    let iconHourClass = ["has-image-centered"];
    if(this.state.addHourClass) {
      iconHourClass.push('active');
    }
    return (
      <div>
        <div style={{textAlign: 'center', marginTop: '50px'}}>
          <h1 className="is-size-3">The Perks of Being Employee at Kolega</h1>
        </div>
        <div className="columns" style={{marginTop: '20px', marginBottom: '50px'}}>
          <div className="column is-6 is-offset-3">
            <div className="columns">
              <div className="column feature-container-big" style={{textAlign:"center"}}>           
                <figure id="feature-cfun" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Culture of fun</p>
                </div>
              </div>
              <div className="column feature-container-big" style={{textAlign:"center"}}>
                <figure id="feature-sdevelopment" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Self development</p>
                </div>
              </div>
              <div className="column feature-container-big" style={{textAlign:"center"}}>
                <figure id="feature-whours" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Flexible working hours</p>
                </div>
              </div>
            </div>
            <div className="columns">
              <div className="column feature-container-big" style={{textAlign:"center"}}>
                <figure id="feature-chub" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Free Collaboration-hub access</p>
                </div>
              </div>
              <div className="column feature-container-big" style={{textAlign:"center"}}>
                <figure id="feature-ffwater" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Free flow water, tea and coffee</p>
                </div>
              </div>
              <div className="column feature-container-big" style={{textAlign:"center"}}>
                <figure id="feature-pdiscounts" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Partnership discounts</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <LinkButton text="Join our team" dest ="/#" /> 
      </div>
    )
  }
}

export default AboutFeatures;
