import React from 'react'
import Image1 from '../../assets/Foto-1.png'
import Image2 from '../../assets/Foto-2.png'
import Image3 from '../../assets/Foto-3.png'

const LocationImages = () => {
  return (
    <div className="columns">
      <div className="column is-10 is-offset-1">
        <div className="columns" id="locationImageOne">
          <div className="column is-half">
            <figure className="image is-4by3">
              <img src={Image1} />
            </figure>
            </div>
            <div className="column is-half" style={{padding: '0.75rem 4rem'}}>
              <h4 style={{color: "rgb(0, 61, 118)", fontWeight: 'bold', textTransform: 'uppercase', fontSize: '2.5rem'}}>Strategic Location</h4>
              <p>
                Located at the heart of Jakarta business district, our locations make it easier for you to have meetings. Also, we are just a fiew minutes away from the Transjakarta and train station
              </p>
            </div>
        </div>

        <div className="columns" id="locationImageTwo">
          <div className="column is-half" style={{padding: '0.75rem 4rem', textAlign: 'right'}} id="imageTwoText">
            <h4 style={{color: "rgb(0, 61, 118)", fontWeight: 'bold', textTransform: 'uppercase', fontSize: '2.5rem'}}>Healthy Lifestyle</h4>
            <p>
              Besides working, you can enjoy several sport activities around our locations. From a simple jogging, basketball, football to badminton. Feel recharged everyday!
            </p>
          </div>
          <div className="column is-half" id="imageTwoImage">
            <figure className="image is-4by3">
              <img src={Image2} />
            </figure>
          </div>
        </div>

        <div className="columns" id="locationImageThree">
          <div className="column is-half">
            <figure className="image is-4by3">
              <img src={Image3} />
            </figure>
          </div>
          <div className="column is-half" style={{padding: '0.75rem 4rem'}}>
            <h4 style={{color: "rgb(0, 61, 118)", fontWeight: 'bold', textTransform: 'uppercase', fontSize: '2.5rem'}}>Flexibility, productive yet comfy</h4>
            <p>
              Connecting with others, especially coworkers, is essential to keep our social-life battery up and running. You can work and grow your business while having yourself surrounded by like-minded individuals. Feel recharged everyday!
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}

export default LocationImages
