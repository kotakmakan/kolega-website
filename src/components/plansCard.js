import React, { Component } from 'react'
import { Card, CardText, CardBody } from 'reactstrap';
import LinkButtonYellow from './linkButtonYellow';

const linkStyle = {
  color: "#FAD02C"
}

class PlansCard extends Component {
  render() {
    const content = this.props.content;
    return (
      <div>
        <Card style={{border: 'none', boxShadow: 'none'}}>
          {/*<CardImg top width="100%"alt="Card image cap"></CardImg>*/}
          <div id="card-headline-container" style={{backgroundImage: `url(${content.headlineImage})`, backgroundPosition: 'center', backgroundSize: '100%', backgroundRepeat: 'no-repeat'}}>
            <div id="card-headline-content" style={{padding: '100px 50px'}}>
              {content.subtitle == '' ? (
                <br />
              ) : (
                <p style={{marginBottom: '0', color: 'white'}}>{content.subtitle}</p>
              )}
              <h4 style={{fontWeight: '700', color: 'white'}}>{content.headlineTitle}</h4>
              <LinkButtonYellow lclass={linkStyle} text={content.linkText} dest={content.linkDest} />
            </div>
          </div>
          
          <CardBody>
            <CardText>{content.intro}</CardText> 
            <div>
              <h3>{content.header}</h3>
              <ul style={{listStyleType:'disc', listStylePosition:'inside'}}>
                {content.texts.map(function(text, i){
                  return <li key={i}>{text}</li>
                })}
              </ul>
              {content.hasOwnProperty("note") && 
                <p>{content.note}</p>}
            </div>
          </CardBody>
        </Card>
      </div>
    )
  }
}

export default PlansCard;
