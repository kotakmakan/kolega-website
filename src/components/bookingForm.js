import React from 'react'

const BookingForm = () => (
  <div>
    <div className="field">
      {/*<label className="label">Name</label> */}
      <div className="control">
        <input className="input" type="text" placeholder="Name" /> 
      </div>
    </div>

    <div className="field">
      {/*<label className="label">Email</label> */}
      <div className="control">
        <input className="input" type="email" placeholder="Email" />
      </div>
    </div>

    <div className="field">
      {/*<label className="label">Phone</label> */}
      <div className="control">
        <input className="input" type="number" placeholder="Phone" />
      </div>
    </div>

    <div className="field">
      {/*<label className="label">Organization</label> */}
      <div className="control">
        <input className="input" type="text" placeholder="Organization" /> 
      </div>
    </div>

    <div className="field">
      {/*<label className="label">Organization</label> */}
      <div className="control">
        <div className="select">
          <select>
            <option>1-5 person</option>
            <option>6-10 person</option>
            <option>11+ person</option>
          </select>  
        </div> 
      </div>
    </div>

  </div>
);

export default BookingForm;