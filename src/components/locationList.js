import React from 'react';
import AliceCarousel from 'react-alice-carousel';
import "react-alice-carousel/lib/alice-carousel.css";
import LocationName from './locationName'

class LocationList extends React.Component {
  responsive = {
    0: { items: 1 },
    600: { items: 2 },
    1024: { items: 4 },
    1600: { items: 4 }
  };

  render() {
    return (
      <div className="columns" style={{backgroundColor:"#ededed"}}>
        <div className="column is-10 is-offset-1">
          <AliceCarousel responsive={this.responsive} dotsDisabled={true}>
            <LocationName name="KOLEGA TEBET" 
              address1="Jl.Tebet Raya No.52"
              address2="(Di atas Comic Cafe)"
              address3="Jakarta Selatan, 12810"
              dest="/kolegatebet"/>
            <LocationName name="KOLEGA x MARKPLUS" 
              address1="Segitiga Emas Business Park"
              address2="Jl. Prof. Dr. Satrio, Kuningan"
              address3="Jakarta Selatan"
              dest="/kolegaxmarkplus"/>
            <LocationName name="KOLEGA ANTASARI" 
              address1="Jl. Pangeran Antasari no.36"
              address2="Puri Sakti Buntu I"
              address3="Jakarta Selatan"
              dest="/kolegaantasari"/>
            <LocationName name="KOLEGA PRIMEDGE" 
              address1="Jl. Tulodong Atas 2"
              address2="Senayan, Kebayoran Baru"
              address3="Jakarta Selatan"
              dest="/kolegaprimedge"/>
            <LocationName name="KOLEGA SENOPATI" 
              address1="Jl. Suryo no. 50, Senopati"
              address2="Senopati, Kebayoran Baru"
              address3="Jakarta Selatan, 12180"
              dest="/kolegasenopati"/>
            <LocationName name="KOLEGA KEMANG" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>
            <LocationName name="KOLEGA BANDUNG" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>
            <LocationName name="JAKARTA, BLOK M" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>     
            <LocationName name="JAKARTA, GATOT SUBROTO" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>    
            <LocationName name="BANDUNG, VETERAN" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="BALI, NUSA DUA" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="PADANG" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="TALAVERA OFFICE PARK" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="SAHID SUDIRMAN CENTER" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="TOKOPEDIA TOWER" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="THE CITY TOWER, THAMRIN" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="TUGU RE BUILDING" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="BANDUNG, NARIPAN" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="AXA TOWER" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/> 
            <LocationName name="JAKARTA, KEMANG TIMUR" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>  
            <LocationName name="JAKARTA, MAS MANSYUR" 
              address1=""
              address2=""
              address3=""
              dest="/kolegaspace"/>   
          </AliceCarousel>
        </div>
      </div>
    )
  }
}

export default LocationList;