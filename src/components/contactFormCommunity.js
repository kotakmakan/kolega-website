import React from 'react'
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';

const ContactformCommunity = () => {
  return (
    <div>
      <Form action="https://formspree.io/info@kolega.co" method="POST" name="_subject" value="New community contact form submission!">
        <FormGroup row>
          <Label for="formCommunityName" sm={2}>Full name</Label>
          <Col sm={10}>
            <Input type="text" name="name" id="formCommunityName" placeholder="Name" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formCommunityPhone"sm={2}>Phone number</Label>
          <Col sm={10}>
            <Input type="number" name="phone" id="formCommunityPhone" placeholder="Phone" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formCommunityEmail"sm={2}>Email</Label>
          <Col sm={10}>
            <Input type="email" name="email" id="formCommunityEmail" placeholder="Email" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formCommunityOrganizationName" sm={2}>Organization name</Label>
          <Col sm={10}>
            <Input type="text" name="organization name" id="formCommunityOrganizationName" placeholder="Organization name" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formCommunityNeeds" sm={2}>Needs</Label>
          <Col sm={10}>
            <Input type="select" name="needs" id="formCommunityNeeds" placeholder="Needs">
              <option>Partnership</option>
              <option>Community</option>
              <option>Others</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formCommunityMessage" sm={2}>Message</Label>
          <Col sm={10}>
            <Input type="textarea" name="message" id="formCommunityMessage" />
          </Col>  
        </FormGroup>
        <Button>SEND MESSAGE</Button>
      </Form>
    </div>
  )
}

export default ContactformCommunity;