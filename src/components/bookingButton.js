import React, { Component } from 'react'
import buttonIcon from '../../assets/icon-book.png'

const buttonStyle = {
  borderStyle: 'solid',
  borderRadius: '50px',
  padding: '0.5rem 1.5rem',
  backgroundColor: 'white',
  borderColor: 'yellow',
  fontFamily: "Lato', sans-serif'",
  fontWeight: '800',
  marginTop: '1rem'
}

export default class BookingButton extends Component {
  render() {
    return (
      <button style={buttonStyle}>
        BOOK NOW <span><img style={{margin: '0 0 0 20px', width: '30px'}} src={buttonIcon} /></span>
      </button>
    )
  }
}
