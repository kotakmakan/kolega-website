import React from 'react'
import Link from 'gatsby-link'
import logo from '../../assets/kolega-logo.png'

const LinkStyle = {
  color: 'white',
  fontSize: '14px'
}

const Menu = () => (
  <nav className="navbar is-dark" role="navigation" aria-label="main navigation">
  <div id="nav-container">
    <div className="navbar-brand">
      <a href="/"><img src={logo} /></a>  
    </div>
    <div className="navbar-menu">
      <div id="navbar-menu-container">
        <div className="navbar-item">
          <Link style={LinkStyle} to="/about">Who we are</Link>
        </div>
        <div className="navbar-item">
          <Link style={LinkStyle} to="/locations">Locations</Link>
        </div>
        <div className="navbar-item">
          <Link style={LinkStyle} to="/plans">Plans</Link>
        </div>
        <div className="navbar-item">
          <Link style={LinkStyle} to="/#">Communities & Partners</Link>
        </div>
        <div className="navbar-item">
          <Link style={LinkStyle} to="/#">What's Happening</Link>
        </div>
        <div className="navbar-item">
          <Link style={LinkStyle} to="/#">Blog</Link>
        </div>
        <div className="navbar-item">
          <Link style={LinkStyle} to="/about">About Us</Link>
        </div>
      </div>  
    </div>
    </div>
  </nav>
  
);

export default Menu;