import React, { Component } from 'react'
import './imageBox.css'
import LinkButton from './linkButton';

class ImageBox extends Component {
  constructor () {
    super()
    this.state = {
      isHidden: true,
      addClass: false
    }
  }

  toggleHidden () {
    this.setState({
      isHidden: !this.state.isHidden,
      addClass: !this.state.addClass
    })
  }

  render() {
    let linkClass = ["imageBoxLink"];
    if(this.state.addClass) {
      linkClass.push('active');
    }
    return (
      <div>
        <div className="imageBoxContainer image is-4by3" onMouseEnter={this.toggleHidden.bind(this)} onMouseLeave={this.toggleHidden.bind(this)}>
          <img src={this.props.image} className="imageBoxImage" />
          {/*{!this.state.isHidden && <Child />}*/}
          <div className='imageBoxModal'>
            <div className="imageBoxText is-size-7" style={{fontFamily: 'RobotoSlab'}}>
              {this.props.text}
            </div>
          </div>
        </div>
      {/*<h4 className={linkClass.join(' ')}>{this.props.linkText}</h4>*/}
      <LinkButton lclass={linkClass.join(' ')} text={this.props.linkText} dest={this.props.linkDest} />
    </div>
    )
  }
}

/*
const Link = () => (
  <div>
    <h4 clasName="imageBoxLink">Coworking Area & All Access Card</h4>
  </div>
)

const Child = () => (
  <div className='imageBoxModal'>
    <div className="imageBoxText">
      Access to the selected, tailor-made, office suite(s) for you and your team, while being able to access the coworking area to huddle-up with the community and colleagues.
    </div>
  </div>
)
*/

export default ImageBox;
