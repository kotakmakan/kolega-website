import React from 'react'
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';

const ContactFormLandlord = () => {
  return (
    <div>
      <Form action="https://formspree.io/info@kolega.co" method="POST" name="_subject" value="New landlord contact form submission!">
        <FormGroup row>
          <Label for="formLandlordName" sm={2}>Full name</Label>
          <Col sm={10}>
            <Input type="text" name="name" id="formLandlordName" placeholder="Name" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordPhone"sm={2}>Phone number</Label>
          <Col sm={10}>
            <Input type="number" name="phone" id="formLandlordPhone" placeholder="Phone" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordEmail"sm={2}>Email</Label>
          <Col sm={10}>
            <Input type="email" name="email" id="formLandlordEmail" placeholder="Email" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordIdentity" sm={2}>Identity status</Label>
          <Col sm={10}>
            <Input type="select" name="identityStatus" id="formLandlordIdentity" placeholder="Property owner">
              <option>Property owner</option>
              <option>Intermediary</option>
              <option>Broker</option>
              <option>Agent property</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordBuildingAddress" sm={2}>Building Address</Label>
          <Col sm={10}>
            <Input type="textarea" name="buildingAddress" id="formLandlordBuildingAddress" />
          </Col>  
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordBuildingCity" sm={2}>City</Label>
          <Col sm={10}>
            <Input type="text" name="buildingCity" id="formLandlordBuildingCity" placeholder="Building City" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordBuildingProvince"sm={2}>Province</Label>
          <Col sm={10}>
            <Input type="text" name="buildingProvince" id="formLandlordBuildingProvince" placeholder="Building Province" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordZipcode"sm={2}>Building Zip Code</Label>
          <Col sm={10}>
            <Input type="number" name="buildingZipCode" id="formLandlordBuildingZipcode" placeholder="Building Zip Code" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordBuildingType" sm={2}>Building Type</Label>
          <Col sm={10}>
            <Input type="select" name="buildingType" id="formLandlordBuildingType" placeholder="Building Type">
              <option>Grade A</option>
              <option>Grade B</option>
              <option>Grade C</option>
              <option>Unique Building</option>
              <option>Shophouse</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordBuildingAvailableSpace"sm={2}>Available Space (in m2)</Label>
          <Col sm={10}>
            <Input type="number" name="buildingAvailableSpace" id="formLandlordBuildingAvailableSpace" placeholder="Available Space" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordBuildingCondition" sm={2}>Condition</Label>
          <Col sm={10}>
            <Input type="select" name="buildingCondition" id="formLandlordBuildingCondition" placeholder="Building Condition">
              <option>Fully Furnished (Furniture & Decoration)</option>
              <option>Semi Furnished (Partitions & flooring/carpets)</option>
              <option>Semi Bare (Ceiling & Concrete floor)</option>
              <option>Bare</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formLandlordSharing" sm={2}>Open for revenue sharing partnership</Label>
          <Col sm={10}>
            <Input type="select" name="revenue sharing partnership" id="formLandlordSharing" placeholder="Revenue sharing partnership">
              <option>Yes</option>
              <option>No</option>
            </Input>
          </Col>
        </FormGroup>
        <Button>SEND MESSAGE</Button>
      </Form>
    </div>
  )
}

export default ContactFormLandlord;