import React, { Component } from 'react'
import BookingButton from './bookingButton'
import EventSpacesImg from '../../assets/Event spaces.png'
import MeetingRoomImg from '../../assets/Meeting room.png'
import MembershipPlansImg from '../../assets/Membership plans.png'

export default class LocationPrice extends Component {
  render() {
    const plansList = this.props.plans;
    const plansPriceList = this.props.plansPrice;
    const roomsList = this.props.rooms;
    const roomsPriceList = this.props.roomsPrice;
    const spacesList = this.props.spaces;
    const spacesPriceList = this.props.spacesPrice;

    return (
      <div className="columns" style={{padding: "0 2.25rem 0 3rem"}}>
        <div className="column" id="locationPriceWrapper">
          <img style={{width: '60%', marginBottom: '20px'}} src={MembershipPlansImg} />
          <div className="columns">
            <div className="column is-half altFont">
              {
                plansList.map(function(name, index){
                  return <div key={ index }>{name}</div>;
                })
              }
            </div>
            <div className="column is-half altFont">
              { 
                plansPriceList.map(function(name, index){
                  return <div key={ index }>{name}</div>;
                })
              }
              <BookingButton />
            </div>
          </div>

          <img style={{width: '50%', marginTop: "40px", marginBottom: '20px'}} src={MeetingRoomImg} />
          <div className="columns">
            <div className="column is-half altFont">
              {
                roomsList.map(function(name, index){
                  return <div key={ index }>{name}</div>;
                })
              }
            </div>
            <div className="column is-half altFont">
              { 
                roomsPriceList.map(function(name, index){
                  return <div key={ index }>{name}</div>;
                })
              }
              <BookingButton />
            </div>
          </div>
          <img style={{width: '50%', marginTop: "40px", marginBottom: '20px'}} src={EventSpacesImg} />
          <div className="columns">
            <div className="column is-half altFont">
              {
                spacesList.map(function(name, index){
                  return <div key={ index }>{name}</div>;
                })
              }
            </div>
            <div className="column is-half altFont">
              { 
                spacesPriceList.map(function(name, index){
                  return <div key={ index }>{name}</div>;
                })
              }
              <BookingButton />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
