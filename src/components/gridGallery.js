import React, { Component } from 'react'
import CoworkingAreaImage from '../../assets/coworking-area.jpg';
import DedicatedDeskImage from '../../assets/dedicated-desk.jpg';
import EventAreaImage from '../../assets/event-area.jpg';
import './gridGallery.css';
import ImageBox from './imageBox'
import { Container, Row, Col } from 'reactstrap';
import BetterPlansImage from '../../assets/better-plans.png'

class GridGallery extends Component {
  constructor(props) {
    super(props);
    this.state = {addClass: false};
  }

  toggle() {
    this.setState({addClass: !this.state.addClass});
  }
  
  handleMouseClick(e) {
    this.activeBox = e;
    console.log(e);
  }

  render(){
    let boxClass = ["imgbox"];
    if(this.state.addClass) {
      boxClass.push('yellow');
    }
    return (
      <div style={{margin: "70px 0"}} id="highlightGrid">
      <div style={{textAlign: 'center', margin: '50px 0'}}>
        <img src={BetterPlansImage} style={{width: '65%'}}/>
      </div>
      <Container>
        <Row>
            <Col md="4">
              <ImageBox
                image={CoworkingAreaImage}
                text="Join Kolega Coworking Space as a member of the growing community. Access to the coworking area in your preferred location with no assigned desk. With one card, you can access Kolega locations anywhere and anytime!
                "
                linkText="Coworking Area & All Access Card"
                linkDest="/"
              />
            </Col>
            <Col md="4">
              <ImageBox
                image={DedicatedDeskImage}
                text="Join Kolega Coworking Space as a member of the growing community. Access to the coworking area in your preferred location with no assigned desk. With one card, you can access Kolega locations anywhere and anytime!
                "
                linkText="Dedicated Desk & Office Suite"
                linkDest="/"
              />
            </Col>
            <Col md="4">
              <ImageBox
                image={EventAreaImage}
                text="We have modular spaces that could accommodate up to 70 persons capacity. In the spaces, you will find inspiration and experience."
                linkText="Meeting Room & Event Space"
                linkDest="/"
              />
            </Col>
        </Row>
      </Container>
    </div>
    );
  }
}


export default GridGallery;
