import React, { Component } from 'react'
import './features.css'

class Features extends Component {
  constructor(props) {
    super()
    this.state = {
      addHourClass: false
    }
  }

  toggleHidden () {
    this.setState({
      addHourClass: !this.state.addClass
    })
  }

  render() {
    let iconHourClass = ["has-image-centered"];
    if(this.state.addHourClass) {
      iconHourClass.push('active');
    }
    return (
      <div>
        <div style={{textAlign: 'center', marginTop: '20px'}}>
          <h1 className="is-size-3">Not just a place, it's a Playground</h1>
        </div>
        <div className="columns" style={{marginTop: '20px'}}>
          <div className="column is-8 is-offset-2">
            <div className="columns">
              <div className="column feature-container" style={{textAlign:"center"}}>           
                <figure id="feature-hour" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>12 hours a day access</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-internet" className="image has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>High speed internet</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-water" className="image is-32x32 has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Free flow mineral water</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-printer" className="image is-32x32 has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Free B/W printer</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="columns" style={{margin: '20px 0 50px 0'}}>
          <div className="column is-10 is-offset-1">
            <div className="columns">
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-locker" className="image is-32x32 has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Personal locker</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-desk" className="image is-32x32 has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Customizable Desk</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-card" className="image is-32x32 has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Exclusive Jejaring access</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-price" className="image is-32x32 has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Special price to other location</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
              <div className="column feature-container" style={{textAlign:"center"}}>
                <figure id="feature-meeting-room" className="image is-32x32 has-image-centered">
                </figure>
                <div>
                  <p className="is-size-5" style={{marginBottom: "0.5rem"}}>Additional meeting room</p>
                  <p className="is-size-7">It is a paradisematic country in which reasted parts of</p>
                </div>
              </div>
            </div>
          </div>
        </div>  
      </div>
    )
  }
}

export default Features;
