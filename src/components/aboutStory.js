import React from 'react'
import aboutStoryImg from '../../assets/about-story.png'
import ourStoryImg from '../../assets/our story.png'

const AboutStory = (props) => {
  return (
    <div className="columns" style={{padding: '40px'}}>
      <div className="column is-three-fifths">
        <img src={aboutStoryImg} />
      </div>
      <div className="column is-two-fifths" style={{position: 'relative'}}>
        <div id="story-container" style={{position: 'absolute', bottom: '0'}}>
          <img src={ourStoryImg} style={{width: '60%', marginBottom: '20px'}} />
          <p dangerouslySetInnerHTML={{__html: props.ourStoryText}}></p>
        </div>
      </div>
    </div>
  )
}

export default AboutStory;
