import React, { Component } from 'react'
import { UncontrolledCarousel } from 'reactstrap'

class Hero extends Component {
  render() {
    return (
      <section className="hero">
        <div className="hero-body" style={{padding: '0'}}>
          <UncontrolledCarousel items={this.props.items} controls={false}/>
        </div>
      </section>
    )
  }
}

export default Hero;