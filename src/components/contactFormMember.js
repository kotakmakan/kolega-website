import React from 'react'
import { Col, Button, Form, FormGroup, Label, Input } from 'reactstrap';

const ContactFormMember = () => {
  return (
    <div>
      <Form action="https://formspree.io/info@kolega.co" method="POST" name="_subject" value="New member contact form submission!">
        <FormGroup row>
          <Label for="formMemberName" sm={2}>Full name</Label>
          <Col sm={10}>
            <Input type="text" name="name" id="formMemberName" placeholder="Name" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formMemberPhone"sm={2}>Phone number</Label>
          <Col sm={10}>
            <Input type="number" name="phone" id="formMemberPhone" placeholder="Phone" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formMemberEmail"sm={2}>Email</Label>
          <Col sm={10}>
            <Input type="email" name="email" id="formMemberEmail" placeholder="Email" />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formMemberOrganization" sm={2}>Organization name</Label>
          <Col sm={10}>
            <Input type="text" name="organization" id="formMemberOrganization" placeholder="Organization" />
          </Col>
        </FormGroup>
         <FormGroup row>
          <Label for="formSelectLocation" sm={2}>Select Location</Label>
          <Col sm={10}>
            <Input type="select" name="locations" id="formSelectLocation" placeholder="Kolega Kemang">
              <option>Kolega Tebet</option>
              <option>Kolega Senopati</option>
              <option>Kolega Antasari</option>
              <option>Kolega Primedge</option>
              <option>Kolega x Markplus</option>
              <option>Kolega Kemang</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formMemberNeeds" sm={2}>Needs</Label>
          <Col sm={10}>
            <Input type="select" name="needs" id="formMemberNeeds" placeholder="Coworking Area">
              <option>Coworking area</option>
              <option>Office suite</option>
              <option>Dedicated desk</option>
              <option>Meeting room</option>
              <option>Event space</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formMemberContactBy" sm={2}>How should we contact you</Label>
          <Col sm={10}>
            <Input type="select" name="contactBy" id="formMemberContactBy" placeholder="Email">
              <option>Email</option>
              <option>Phone</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="formMemberMessage" sm={2}>Message</Label>
          <Col sm={10}>
            <Input type="textarea" name="message" id="formMemberMessage" />
          </Col>  
        </FormGroup>
        <Button>SEND MESSAGE</Button>
      </Form>
    </div>
  )
}

export default ContactFormMember;