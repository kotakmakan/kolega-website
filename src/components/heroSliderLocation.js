import React, { Component } from 'react';
import { Jumbotron, 
  Button,   
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption } from 'reactstrap';

import LocationHeroImage from '../../assets/header-location/Header Location 1.jpg'
import LocationHeroImage2 from '../../assets/header-location/Header Location 2.jpg'
import LocationHeroImage3 from '../../assets/header-location/Header Location 3.jpg'
import './heroSliderLocation.css'


const items = [
  {
    src: LocationHeroImage,
    altText: 'Slide 1',
    /*header: "Let's Work Together",
    caption: 'Work amongst the community to stimulate growth and collaboration, while having the choice to have your own private space to do your works'*/
    header: "",
    caption:""
  },
  {
    src: LocationHeroImage2,
    altText: 'Slide 2',
    /*header: "Let's Work Together",
    caption: 'Work amongst the community to stimulate growth and collaboration, while having the choice to have your own private space to do your works'*/
    header: "",
    caption:""
  },
  {
    src: LocationHeroImage3,
    altText: 'Slide 3',
    /*header: "Let's Work Together",
    caption: 'Work amongst the community to stimulate growth and collaboration, while having the choice to have your own private space to do your works'*/
    header: "",
    caption:""
  }
];

class HeroSliderLocation extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img src={item.src} alt={item.altText} />
          <CarouselCaption captionText={item.caption} captionHeader={item.header} />
        </CarouselItem>
      );
    });

    return (
      <div>
        <Jumbotron style={{padding: '0'}}>
          <Carousel
            activeIndex={activeIndex}
            next={this.next}
            previous={this.previous}
            className="leftCaption"
          >
            <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
            {slides}
            {/*<CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
            <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} /> */}
          </Carousel>
        </Jumbotron> 
      </div>
    );
  }

};

export default HeroSliderLocation;