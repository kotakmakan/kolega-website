import React from 'react'

const SmallHero = () => (
  <section className="hero is-primary is-medium">
    <div className="hero-body">
      <div className="container has-text-centered">
        <h1 className="title">
          Contact us now and get a discount
        </h1>
        <h2 className="subtitle">
          How to reach us
        </h2>
      </div>
    </div>
  </section>
);

export default SmallHero;
