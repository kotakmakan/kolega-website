import React from 'react'
import InstagramIcon from '../../assets/icons/instagram-logo.png'
import MailIcon from '../../assets/icons/close-envelope.png'
import TwitterIcon from '../../assets/icons/twitter-logo-silhouette.png'
import LinkedInIcon from '../../assets/icons/linkedin-logo.png'


const hrStyle = {
  display: 'block',
  height: '1px',
  border: '0',
  borderTop: '1px solid #ccc',
  margin: '1em 60px',
  padding: '0'
}

const Footer = () => (
  <div style={{backgroundColor:"#ededed"}}> 
    <div className="columns" style={{padding: "0 80px"}}>
      <div className="column">
        <p>More info</p>
        <h1>About Kolega</h1>
        <p className="is-size-7">We are a place for energetic, like-minded and 
          <br />enthusiastic people who are aiming to grow together. 
          <br />Creating an ecosystem that embodies friendship 
          <br />as the fuel of productivity and collaboration.</p>
      </div>
      <div id="footer-contact" className="column">
        <div className="has-text-right">
          <p>Contacts</p>
          <p className="is-size-7">Jl. Tebet Raya No. 52<br />Jakarta Selatan, 1218</p>
          <p className="is-size-7"><a href="mailto:info@kolega.co" style={{color: '#003d76'}}>info@kolega.co</a></p>
        </div>
        <div className="level" id="footer-social-media" style={{justifyContent: 'flex-end'}}>
          <div className="level-right">
            <a href="mailto:info@kolega.co" className="level-item"><figure className="image is-16x16"><img src={MailIcon} /></figure></a>
            <a href="http://www.twitter.com/kolegaco" className="level-item"><figure className="image is-16x16"><img src={TwitterIcon} /></figure></a>
            <a href="http://www.linkedin.com/company/kolega-co-working-space/" className="level-item"><figure className="image is-16x16"><img src={LinkedInIcon} /></figure></a>
            <a href="http://www.instagram.com/kolegaco" className="level-item"><figure className="image is-16x16"><img src={InstagramIcon} /></figure></a>
          </div>
        </div>
      </div>
      
    </div>
    <hr style={hrStyle}/>
    <div className="columns" style={{padding: "0 80px"}}>
      <div className="column">
        <p className="is-size-7">&copy; 2018 by Kolega Coworking Space Indonesia</p>
      </div>
      <div className="column has-text-right">
        <p className="is-size-7">Privacy Policy. Terms of Use. Site Map</p>
      </div>
    </div>
  </div>  
)

export default Footer;
