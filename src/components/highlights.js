import React, { Component } from 'react';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';
import AffordabilityImg from '../../assets/value-slider/Affordability.jpg';
import ConnectivityImg from '../../assets/value-slider/Connectivity.jpg';
import FlexibilityImg from '../../assets/value-slider/Flexibility.jpg';
import LinkButton from './linkButton';

const items = [
  {
    src: FlexibilityImg,
    altText: 'Slide 1',
    caption: 'Slide 1'
  },
  {
    src: AffordabilityImg,
    altText: 'Slide 2',
    caption: 'Slide 2'
  },
  {
    src: ConnectivityImg,
    altText: 'Slide 3',
    caption: 'Slide 3'
  }
];

let displayTexts = [
  {
    displayTextHeader: 'Flexibility',
    displayText: 'Maximize your productivity by Kolega All Access Card and you can work flexibly in our locations;',
    displayTextLink :'Browse Locations',
    displayTextDest : '/locations'
  },
  {
    displayTextHeader: 'Affordability',
    displayText: 'No more high cost office with many unused space. Pay more efficient for spaces that matter to you!',
    displayTextLink :'Check Our Plans',
    displayTextDest : '/'
  },
  {
    displayTextHeader: 'Connectivity',
    displayText: 'Be ready for future collaboration and widen up your connections by joining event collaboration with Kolega partners.',
    displayTextLink :'Community & Members',
    displayTextDest : '/'
  }
]

const captionStyle = {
  position: 'relative',
  color: 'black'
}

class Highlight extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
    this.captions = ["nomor 0", "nomor 1", "nomor 2"];
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img src={item.src} alt={item.altText} />
          {/*<CarouselCaption captionText={item.caption} captionHeader={item.caption} style={captionStyle} />*/}
        </CarouselItem>
      );
    });

    var displayText = ''
    if (this.state.activeIndex == 0) {
      //displayTextHeader
      displayText = "nomor 0";
    } else if (this.state.activeIndex == 1) {
      displayText = "nomor 1";
    } else {
      displayText = "nomor 2";
    }

    return (
      <div className="columns level">
        <div className="column level-item is-two-fifths has-text-centered">
          <div id="highlight-caption">
            <h1 className="is-size-3">{displayTexts[activeIndex].displayTextHeader}</h1>
            <p>{displayTexts[activeIndex].displayText}</p>
            <LinkButton dest={displayTexts[activeIndex].displayTextDest} text={displayTexts[activeIndex].displayTextLink}/>
          </div>
        </div>
        <div className="column is-three-fifths">
          <Carousel
            activeIndex={activeIndex}
            next={this.next}
            previous={this.previous}
          >
            <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
            {slides}
            <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
            <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
          </Carousel>
        </div>
      </div>
    );
  }
}


export default Highlight;