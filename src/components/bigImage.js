import React from 'react'

const BigImage = (props) => {
  return (
    <div style={{padding: '40px', backgroundColor:'#F7F7F7' }}>
      <figure className="image">
        <img src={props.bigImageSrc} />
      </figure>
    </div>
  )
}

export default BigImage;
