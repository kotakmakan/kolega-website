import React from 'react'
import ourTeamImg from '../../assets/our team.png'
import AboutFeatures from './aboutFeature';

const AboutTeam = (props) => {
  return (
    <div className="columns" style={{backgroundColor: '#F7F7F7'}}>
      <div className="column is-8 is-offset-2 has-text-centered" style={{padding: '50px 10px 70px 10px'}}>
        <img src={ourTeamImg} style={{marginBottom: '20px', width:'30%'}}/>
        <p dangerouslySetInnerHTML={{__html: props.ourTeamText}}></p>
        <AboutFeatures />
      </div>
    </div>
  )
}

export default AboutTeam;
