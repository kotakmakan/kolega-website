import React, { Component } from 'react'

class LocationOffer extends Component {
  render() {
    return (
      <div className="columns" id="locationOfferWrapper">
        <div className="column is-10 is-offset-1">
          <div className="columns">
            <div className="column is-one-third" id="locationOfferHeader">
              <h1 style={{fontWeight: '1000'}} className="is-size-3">We are Offering the<br />
              <span style={{color:"darkblue"}}>Best Office Deals</span></h1>
            </div>
            <div className="column is-two-third" id="locationOfferText">
              <p className="is-size-6 altFont" style={{marginTop: '6px', lineHeight:'1.85'}}>{this.props.description}</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default LocationOffer;