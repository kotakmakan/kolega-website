import React, { Component } from 'react'
import Link from 'gatsby-link'
import LinkIcon from '../../assets/link-icon.png'

const LinkStyle = {
  color: 'black'
}

export default class LinkButton extends Component {
  render() {
    return (
      <Link style={LinkStyle} to={this.props.dest}>
        <div className={this.props.lclass}>{this.props.text}<span style={{marginLeft: '5px'}}><img src={LinkIcon} /></span></div>  
      </Link> 
    )
  }
}
