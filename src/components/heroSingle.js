import React from 'react'

const HeroSingle = (props) => {
  return (
    <section className="hero is-primary is-large">
      <div className="hero-body" id="hero-single-body" style={{backgroundImage: `url(${props.backgroundImage})`, backgroundPosition: 'center', backgroundSize: 'cover'}}>
        <div className="container" id="hero-single-container">
          <h1 className="title">
            {props.title}
          </h1>
          <h2 className="subtitle">
            {props.sbtitle}
          </h2>
        </div>
      </div>
    </section>
  )
}

export default HeroSingle;