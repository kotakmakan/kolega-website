import React, { Component } from 'react'

const listStyle = {
  borderStyle: 'solid',
  borderColor: '#637c90', 
  width:'95%', 
  paddingTop: '60px',
  paddingLeft: '10px'
}

class LocationName extends Component {
  render() {
    return (
      <a href={this.props.dest}>
      <div style={listStyle} className="location-name">
        <h5 className="is-size-6">{this.props.name}</h5>
        <p style={{marginBottom: '0.5rem'}} className="is-size-7">{this.props.address1}<br />
        {this.props.address2}<br />
        {this.props.address3}</p>
      </div>
      </a>
    )
  }
}

export default LocationName;
