import React from 'react'
import LinkButton from './linkButton';

const PlansCardHeadline = (props) => {
  return (
    <div id="card-headline-container" style={{backgroundImage: `url(${props.headlineImage})`}}>
      <div id="card-headline-content">
        <p>{props.headlineSubtitle}</p>
        <h4>{props.headlineTitle}</h4>
        <LinkButton dest={props.headlineDest} />
      </div>
    </div>
  )
}

export default PlansCardHeadline;
