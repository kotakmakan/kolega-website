# gatsby-starter-default
The default Gatsby starter.

For an overview of the project structure please refer to the [Gatsby documentation - Building with Components](https://www.gatsbyjs.org/docs/building-with-components/).

## Install

Make sure that you have the Gatsby CLI program installed:
```sh
npm install --global gatsby-cli
```

Then you can run it by:
```sh
gatsby develop
```
The website will be available in `localhost:8000`

## Build

To build into static html, execute the following command:
```sh
gatsby build
```

This will create static HTML CSS and JS files
## Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/kotakmakan/kolega-website)
