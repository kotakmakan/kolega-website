module.exports = {
  siteMetadata: {
    title: 'Kolega',
  },
  plugins: [
    'gatsby-plugin-react-helmet', 
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-source-prismic',
      options: {
        repositoryName: 'kolega-website',
        accessToken: 'MC5XMkVqemlBQUFDQUFGUGhD.77-977-977-9aUjvv71wO1nvv71FCu-_vQlreHvvv73vv73vv73vv71L77-9Snrvv71QBlk0O--_vQ',
        linkResolver: ({ node, key, value }) => doc => {
          '/${doc.uid}'
        },
        htmlSerializer: ({ node, key, value }) => (
          (type, element, content, children) => {

          }
        )
      }
    }
  ],
}
